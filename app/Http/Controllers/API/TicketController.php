<?php

namespace App\Http\Controllers\API;

use App\Enums\SeverityType;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Ticket;
use Validator;
use App\Http\Resources\Ticket as TicketResource;

class TicketController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $tickets = Ticket::where("user_id", "=", $user->id)->get();

        return $this->sendResponse(TicketResource::collection($tickets), 'Tickets retrieved successfully.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $input = $request->all();
        $input['user_id'] = $user->id;

        $validator = Validator::make($input, [
            'description' => 'required',
            'severity' => ['required', new EnumValue(SeverityType::class)],

        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $ticket = Ticket::create($input);

        return $this->sendResponse(new TicketResource($ticket), 'Ticket created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Ticket::find($id);

        if (is_null($ticket)) {
            return $this->sendError('Ticket not found.');
        }

        return $this->sendResponse(new TicketResource($ticket), 'Ticket retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'description' => 'required',
            'severity' => ['required', new EnumValue(SeverityType::class)],
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $ticket->description = $input['description'];
        $ticket->severity = $input['severity'];
        $ticket->save();

        return $this->sendResponse(new TicketResource($ticket), 'Ticket updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        $ticket->delete();

        return $this->sendResponse([], 'Ticket deleted successfully.');
    }
}