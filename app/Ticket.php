<?php

namespace App;

use App\Enums\SeverityType;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'severity', 'user_id'
    ];

    public function user()
    {
        $this->belongsTo(User::class, 'user_id');
    }
}